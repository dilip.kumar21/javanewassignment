package javaassignment;

import java.util.LinkedHashSet;
import java.util.Set;

public class RemoveStringDuplicates {
	public static void main(String[] args) {
		String st = "Java python java Python java sql Java";
		String[] splt = st.split(" ");
		Set<String> se = new LinkedHashSet<>();
		for (String str : splt) {
			se.add(str);
		}
		System.out.println(se);
	}
}