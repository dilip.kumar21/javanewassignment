package javaassignment;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class OccuranceOfValueinList {
	public static void main(String[] args) {
		List<Integer> li = Arrays.asList(10, 40, 20, 50, 10, 20, 30,10);
		Map<Integer, Integer> mp = new LinkedHashMap<>();
		for (Integer value : li) {
			if (mp.containsKey(value)) {
				Integer count = mp.get(value);
				mp.put(value, count + 1);

			} else {
				mp.put(value, 1);
			}
		}
		System.out.println(mp);

	}

}
